#Django Favorites

My generic favorites framework for Django fork.

This fork differentiates from it's parent mainly in front-end:

* it uses a single view to process ajax fav/unfav (like/unlike) action
* has a single templatetag to render a fav/unfav button
* includes a jquery function to handle the ajax button click


##Installation

- ./setup.py install
  or copy /favorites to your project directory
- add 'favorites' to your INSTALLED_APPS
- add favorites.urls to your urls.py


##Basic usage examples:

###Render a fav/unfav button in your template and provide csrf_token

```
{% load favorite_tags %}
{% csrf_token %}
{% for post in blog_posts %}
<h3>{{ post.title }}</h3>
<p>{{ post.content }}</p>
<div class="actions">{% if user.is_authenticated %}{% fav_item post user %}{% endif %}</div>
{% endfor %}
```
###Get all favorites for user
```
favs =  Favorite.objects.favorites_for_user(user)
```
###Get only blogpost favorites for user
```
content_type = get_object_or_404(ContentType, app_label='myblogapp', model='blogpost')
favs = Favorite.objects.favorites_for_user(user).filter(content_type=content_type)
```

You can configure the text messages overriding these values in your [settings.py](https://github.com/last_partizan/django-favorites/blob/master/favorites/settings.py) or override the [template](https://github.com/last_partizan/django-favorites/blob/master/favorites/templates/favorites/fav_item.html)


