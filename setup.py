from distutils.core import setup

setup(
    name = 'favorites',
    version = '0.3',
    description = 'Generic favorites application for Django',
    author = 'Sergey Tereschenko',
    author_email = 'serg.partizan@gmail.com',
    packages = ['favorites', 'favorites.templatetags'],
    package_data={'favorites': ['static/*/*', 'templates/*/*']},
    classifiers = ['Development Status :: 4 - Beta',
                   'Environment :: Web Environment',
                   'Framework :: Django',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Topic :: Utilities'],
)
